# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'MainWindow.ui'
##
## Created by: Qt User Interface Compiler version 6.4.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QComboBox, QGridLayout, QHBoxLayout,
    QLabel, QLineEdit, QMainWindow, QSizePolicy,
    QSplitter, QTabWidget, QToolButton, QVBoxLayout,
    QWidget)
import icon_rc

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1237, 806)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.centralwidget.setStyleSheet(u"#centralwidget{\n"
"	background-color: rgb(0, 170, 255);\n"
"}")
        self.verticalLayout = QVBoxLayout(self.centralwidget)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.tabWidgetModels = QTabWidget(self.centralwidget)
        self.tabWidgetModels.setObjectName(u"tabWidgetModels")
        self.tabWidgetModels.setContextMenuPolicy(Qt.NoContextMenu)
        self.tabWidgetModels.setStyleSheet(u"QTabWidget#tabWidgetModels>QTabBar::tab{\n"
"width: 150;\n"
"height:30;\n"
"}\n"
"QTabWidget>QTabBar::tab:!selected{\n"
"	background-color: rgb(243, 243, 243);\n"
"}")
        self.tabWidgetModels.setTabShape(QTabWidget.Rounded)
        self.tabWidgetModels.setElideMode(Qt.ElideLeft)
        self.tabWidgetModels.setUsesScrollButtons(True)
        self.tabWidgetModels.setDocumentMode(True)
        self.tabWidgetModels.setTabsClosable(False)
        self.tabWidgetModels.setMovable(False)
        self.tabWidgetModels.setTabBarAutoHide(False)
        self.pageModel_1 = QWidget()
        self.pageModel_1.setObjectName(u"pageModel_1")
        self.pageModel_1.setStyleSheet(u"")
        self.gridLayout_17 = QGridLayout(self.pageModel_1)
        self.gridLayout_17.setSpacing(0)
        self.gridLayout_17.setObjectName(u"gridLayout_17")
        self.gridLayout_17.setContentsMargins(0, 0, 0, 0)
        self.splitter = QSplitter(self.pageModel_1)
        self.splitter.setObjectName(u"splitter")
        self.splitter.setOrientation(Qt.Horizontal)
        self.splitter.setHandleWidth(2)
        self.splitter.setChildrenCollapsible(False)
        self.tabWidgetSide_4 = QTabWidget(self.splitter)
        self.tabWidgetSide_4.setObjectName(u"tabWidgetSide_4")
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tabWidgetSide_4.sizePolicy().hasHeightForWidth())
        self.tabWidgetSide_4.setSizePolicy(sizePolicy)
        self.tabWidgetSide_4.setMinimumSize(QSize(202, 0))
        self.tabWidgetSide_4.setAutoFillBackground(False)
        self.tabWidgetSide_4.setTabPosition(QTabWidget.West)
        self.widgetSide_7 = QWidget()
        self.widgetSide_7.setObjectName(u"widgetSide_7")
        self.tabWidgetSide_4.addTab(self.widgetSide_7, "")
        self.widgetSide_8 = QWidget()
        self.widgetSide_8.setObjectName(u"widgetSide_8")
        self.tabWidgetSide_4.addTab(self.widgetSide_8, "")
        self.splitter.addWidget(self.tabWidgetSide_4)
        self.widget_4 = QWidget(self.splitter)
        self.widget_4.setObjectName(u"widget_4")
        sizePolicy1 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy1.setHorizontalStretch(4)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.widget_4.sizePolicy().hasHeightForWidth())
        self.widget_4.setSizePolicy(sizePolicy1)
        self.gridLayout_2 = QGridLayout(self.widget_4)
        self.gridLayout_2.setSpacing(0)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.splitter_14 = QSplitter(self.widget_4)
        self.splitter_14.setObjectName(u"splitter_14")
        self.splitter_14.setOrientation(Qt.Vertical)
        self.splitter_14.setHandleWidth(2)
        self.splitter_14.setChildrenCollapsible(False)
        self.splitter_15 = QSplitter(self.splitter_14)
        self.splitter_15.setObjectName(u"splitter_15")
        sizePolicy2 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(5)
        sizePolicy2.setHeightForWidth(self.splitter_15.sizePolicy().hasHeightForWidth())
        self.splitter_15.setSizePolicy(sizePolicy2)
        self.splitter_15.setOrientation(Qt.Horizontal)
        self.splitter_15.setOpaqueResize(True)
        self.splitter_15.setHandleWidth(1)
        self.splitter_15.setChildrenCollapsible(False)
        self.splitter_16 = QSplitter(self.splitter_15)
        self.splitter_16.setObjectName(u"splitter_16")
        self.splitter_16.setOrientation(Qt.Vertical)
        self.splitter_16.setHandleWidth(2)
        self.splitter_16.setChildrenCollapsible(False)
        self.tabWidget_17 = QTabWidget(self.splitter_16)
        self.tabWidget_17.setObjectName(u"tabWidget_17")
        self.tabWidget_17.setStyleSheet(u"QTabWidget#tabWidget_5>QTabBar::tab{\n"
"height:18px\n"
"}")
        self.tabWidget_17.setMovable(False)
        self.tab_33 = QWidget()
        self.tab_33.setObjectName(u"tab_33")
        self.tabWidget_17.addTab(self.tab_33, "")
        self.tab_34 = QWidget()
        self.tab_34.setObjectName(u"tab_34")
        self.tabWidget_17.addTab(self.tab_34, "")
        self.splitter_16.addWidget(self.tabWidget_17)
        self.splitter_15.addWidget(self.splitter_16)
        self.splitter_17 = QSplitter(self.splitter_15)
        self.splitter_17.setObjectName(u"splitter_17")
        self.splitter_17.setOrientation(Qt.Vertical)
        self.splitter_17.setHandleWidth(2)
        self.splitter_17.setChildrenCollapsible(False)
        self.tabWidget_19 = QTabWidget(self.splitter_17)
        self.tabWidget_19.setObjectName(u"tabWidget_19")
        self.tabWidget_19.setStyleSheet(u"QTabWidget#tabWidget_5>QTabBar::tab{\n"
"height:18px\n"
"}")
        self.tabWidget_19.setMovable(False)
        self.tab_37 = QWidget()
        self.tab_37.setObjectName(u"tab_37")
        self.tabWidget_19.addTab(self.tab_37, "")
        self.tab_38 = QWidget()
        self.tab_38.setObjectName(u"tab_38")
        self.tabWidget_19.addTab(self.tab_38, "")
        self.splitter_17.addWidget(self.tabWidget_19)
        self.splitter_15.addWidget(self.splitter_17)
        self.splitter_14.addWidget(self.splitter_15)
        self.tabWidget_4 = QTabWidget(self.splitter_14)
        self.tabWidget_4.setObjectName(u"tabWidget_4")
        sizePolicy3 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(2)
        sizePolicy3.setHeightForWidth(self.tabWidget_4.sizePolicy().hasHeightForWidth())
        self.tabWidget_4.setSizePolicy(sizePolicy3)
        self.tab_7 = QWidget()
        self.tab_7.setObjectName(u"tab_7")
        self.tabWidget_4.addTab(self.tab_7, "")
        self.tab_8 = QWidget()
        self.tab_8.setObjectName(u"tab_8")
        self.tabWidget_4.addTab(self.tab_8, "")
        self.splitter_14.addWidget(self.tabWidget_4)

        self.gridLayout_2.addWidget(self.splitter_14, 0, 0, 1, 1)

        self.splitter.addWidget(self.widget_4)

        self.gridLayout_17.addWidget(self.splitter, 1, 0, 1, 1)

        self.widgetUp = QWidget(self.pageModel_1)
        self.widgetUp.setObjectName(u"widgetUp")
        sizePolicy4 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy4.setHorizontalStretch(0)
        sizePolicy4.setVerticalStretch(0)
        sizePolicy4.setHeightForWidth(self.widgetUp.sizePolicy().hasHeightForWidth())
        self.widgetUp.setSizePolicy(sizePolicy4)
        self.widgetUp.setMinimumSize(QSize(0, 80))
        self.horizontalLayout_3 = QHBoxLayout(self.widgetUp)
        self.horizontalLayout_3.setSpacing(2)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalLayout_3.setContentsMargins(2, 2, 2, 2)
        self.widgetGlobal_3 = QWidget(self.widgetUp)
        self.widgetGlobal_3.setObjectName(u"widgetGlobal_3")
        sizePolicy5 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        sizePolicy5.setHorizontalStretch(0)
        sizePolicy5.setVerticalStretch(0)
        sizePolicy5.setHeightForWidth(self.widgetGlobal_3.sizePolicy().hasHeightForWidth())
        self.widgetGlobal_3.setSizePolicy(sizePolicy5)
        self.widgetGlobal_3.setMinimumSize(QSize(200, 0))
        self.widgetGlobal_3.setStyleSheet(u"background-color: rgb(255, 255, 255);")
        self.gridLayout_13 = QGridLayout(self.widgetGlobal_3)
        self.gridLayout_13.setObjectName(u"gridLayout_13")
        self.comboBox_3 = QComboBox(self.widgetGlobal_3)
        self.comboBox_3.setObjectName(u"comboBox_3")
        self.comboBox_3.setAutoFillBackground(False)

        self.gridLayout_13.addWidget(self.comboBox_3, 0, 1, 1, 1)

        self.label_17 = QLabel(self.widgetGlobal_3)
        self.label_17.setObjectName(u"label_17")

        self.gridLayout_13.addWidget(self.label_17, 2, 0, 1, 1)

        self.label_16 = QLabel(self.widgetGlobal_3)
        self.label_16.setObjectName(u"label_16")

        self.gridLayout_13.addWidget(self.label_16, 1, 0, 1, 1)

        self.lineEdit_3 = QLineEdit(self.widgetGlobal_3)
        self.lineEdit_3.setObjectName(u"lineEdit_3")

        self.gridLayout_13.addWidget(self.lineEdit_3, 1, 1, 1, 1)

        self.label_15 = QLabel(self.widgetGlobal_3)
        self.label_15.setObjectName(u"label_15")

        self.gridLayout_13.addWidget(self.label_15, 0, 0, 1, 1)

        self.label_18 = QLabel(self.widgetGlobal_3)
        self.label_18.setObjectName(u"label_18")

        self.gridLayout_13.addWidget(self.label_18, 2, 1, 1, 1)


        self.horizontalLayout_3.addWidget(self.widgetGlobal_3)

        self.widgetTool_7 = QWidget(self.widgetUp)
        self.widgetTool_7.setObjectName(u"widgetTool_7")
        sizePolicy5.setHeightForWidth(self.widgetTool_7.sizePolicy().hasHeightForWidth())
        self.widgetTool_7.setSizePolicy(sizePolicy5)
        self.widgetTool_7.setStyleSheet(u"background-color: rgb(255, 255, 255);")
        self.gridLayout_14 = QGridLayout(self.widgetTool_7)
        self.gridLayout_14.setSpacing(0)
        self.gridLayout_14.setObjectName(u"gridLayout_14")
        self.gridLayout_14.setContentsMargins(0, 0, 0, 0)
        self.toolButton_15 = QToolButton(self.widgetTool_7)
        self.toolButton_15.setObjectName(u"toolButton_15")
        self.toolButton_15.setMinimumSize(QSize(60, 60))
        icon = QIcon()
        icon.addFile(u":/icon/icon/folder.svg", QSize(), QIcon.Normal, QIcon.On)
        self.toolButton_15.setIcon(icon)
        self.toolButton_15.setIconSize(QSize(30, 30))
        self.toolButton_15.setPopupMode(QToolButton.MenuButtonPopup)
        self.toolButton_15.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolButton_15.setAutoRaise(True)

        self.gridLayout_14.addWidget(self.toolButton_15, 0, 0, 1, 1)

        self.toolButton_16 = QToolButton(self.widgetTool_7)
        self.toolButton_16.setObjectName(u"toolButton_16")
        self.toolButton_16.setMinimumSize(QSize(60, 60))
        icon1 = QIcon()
        icon1.addFile(u":/icon/icon/save.svg", QSize(), QIcon.Normal, QIcon.On)
        self.toolButton_16.setIcon(icon1)
        self.toolButton_16.setIconSize(QSize(30, 30))
        self.toolButton_16.setPopupMode(QToolButton.MenuButtonPopup)
        self.toolButton_16.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolButton_16.setAutoRaise(True)

        self.gridLayout_14.addWidget(self.toolButton_16, 0, 1, 1, 1)

        self.label_19 = QLabel(self.widgetTool_7)
        self.label_19.setObjectName(u"label_19")
        font = QFont()
        font.setFamilies([u"\u5fae\u8f6f\u96c5\u9ed1 Light"])
        font.setPointSize(8)
        self.label_19.setFont(font)
        self.label_19.setAlignment(Qt.AlignCenter)

        self.gridLayout_14.addWidget(self.label_19, 1, 0, 1, 2)


        self.horizontalLayout_3.addWidget(self.widgetTool_7)

        self.widgetTool_8 = QWidget(self.widgetUp)
        self.widgetTool_8.setObjectName(u"widgetTool_8")
        sizePolicy5.setHeightForWidth(self.widgetTool_8.sizePolicy().hasHeightForWidth())
        self.widgetTool_8.setSizePolicy(sizePolicy5)
        self.widgetTool_8.setStyleSheet(u"background-color: rgb(255, 255, 255);")
        self.gridLayout_15 = QGridLayout(self.widgetTool_8)
        self.gridLayout_15.setSpacing(0)
        self.gridLayout_15.setObjectName(u"gridLayout_15")
        self.gridLayout_15.setContentsMargins(0, 0, 0, 0)
        self.toolButton_17 = QToolButton(self.widgetTool_8)
        self.toolButton_17.setObjectName(u"toolButton_17")
        self.toolButton_17.setMinimumSize(QSize(60, 60))
        icon2 = QIcon()
        icon2.addFile(u":/icon/icon/edit-1.svg", QSize(), QIcon.Normal, QIcon.On)
        self.toolButton_17.setIcon(icon2)
        self.toolButton_17.setIconSize(QSize(30, 30))
        self.toolButton_17.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolButton_17.setAutoRaise(True)

        self.gridLayout_15.addWidget(self.toolButton_17, 0, 1, 1, 1)

        self.toolButton_18 = QToolButton(self.widgetTool_8)
        self.toolButton_18.setObjectName(u"toolButton_18")
        self.toolButton_18.setMinimumSize(QSize(60, 60))
        icon3 = QIcon()
        icon3.addFile(u":/icon/icon/play-circle-stroke.svg", QSize(), QIcon.Normal, QIcon.On)
        self.toolButton_18.setIcon(icon3)
        self.toolButton_18.setIconSize(QSize(30, 30))
        self.toolButton_18.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolButton_18.setAutoRaise(True)

        self.gridLayout_15.addWidget(self.toolButton_18, 0, 0, 1, 1)

        self.label_20 = QLabel(self.widgetTool_8)
        self.label_20.setObjectName(u"label_20")
        self.label_20.setFont(font)
        self.label_20.setAlignment(Qt.AlignCenter)

        self.gridLayout_15.addWidget(self.label_20, 1, 0, 1, 3)


        self.horizontalLayout_3.addWidget(self.widgetTool_8)

        self.widgetTool_9 = QWidget(self.widgetUp)
        self.widgetTool_9.setObjectName(u"widgetTool_9")
        sizePolicy5.setHeightForWidth(self.widgetTool_9.sizePolicy().hasHeightForWidth())
        self.widgetTool_9.setSizePolicy(sizePolicy5)
        self.widgetTool_9.setStyleSheet(u"background-color: rgb(255, 255, 255);")
        self.gridLayout_16 = QGridLayout(self.widgetTool_9)
        self.gridLayout_16.setSpacing(0)
        self.gridLayout_16.setObjectName(u"gridLayout_16")
        self.gridLayout_16.setContentsMargins(0, 0, 0, 0)
        self.label_21 = QLabel(self.widgetTool_9)
        self.label_21.setObjectName(u"label_21")
        self.label_21.setFont(font)
        self.label_21.setAlignment(Qt.AlignCenter)

        self.gridLayout_16.addWidget(self.label_21, 1, 0, 1, 4)

        self.toolButton_19 = QToolButton(self.widgetTool_9)
        self.toolButton_19.setObjectName(u"toolButton_19")
        self.toolButton_19.setMinimumSize(QSize(60, 60))
        icon4 = QIcon()
        icon4.addFile(u":/icon/icon/setting.svg", QSize(), QIcon.Normal, QIcon.On)
        self.toolButton_19.setIcon(icon4)
        self.toolButton_19.setIconSize(QSize(30, 30))
        self.toolButton_19.setPopupMode(QToolButton.DelayedPopup)
        self.toolButton_19.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolButton_19.setAutoRaise(True)

        self.gridLayout_16.addWidget(self.toolButton_19, 0, 0, 1, 1)

        self.toolButton_20 = QToolButton(self.widgetTool_9)
        self.toolButton_20.setObjectName(u"toolButton_20")
        self.toolButton_20.setMinimumSize(QSize(60, 60))
        icon5 = QIcon()
        icon5.addFile(u":/icon/icon/help-circle.svg", QSize(), QIcon.Normal, QIcon.On)
        self.toolButton_20.setIcon(icon5)
        self.toolButton_20.setIconSize(QSize(30, 30))
        self.toolButton_20.setPopupMode(QToolButton.DelayedPopup)
        self.toolButton_20.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolButton_20.setAutoRaise(True)

        self.gridLayout_16.addWidget(self.toolButton_20, 0, 3, 1, 1)

        self.toolButton_21 = QToolButton(self.widgetTool_9)
        self.toolButton_21.setObjectName(u"toolButton_21")
        self.toolButton_21.setMinimumSize(QSize(60, 60))
        icon6 = QIcon()
        icon6.addFile(u":/icon/icon/clear.svg", QSize(), QIcon.Normal, QIcon.On)
        self.toolButton_21.setIcon(icon6)
        self.toolButton_21.setIconSize(QSize(30, 30))
        self.toolButton_21.setPopupMode(QToolButton.DelayedPopup)
        self.toolButton_21.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolButton_21.setAutoRaise(True)

        self.gridLayout_16.addWidget(self.toolButton_21, 0, 1, 1, 1)


        self.horizontalLayout_3.addWidget(self.widgetTool_9)

        self.widgetToolSpace_3 = QWidget(self.widgetUp)
        self.widgetToolSpace_3.setObjectName(u"widgetToolSpace_3")
        self.widgetToolSpace_3.setStyleSheet(u"background-color: rgb(255, 255, 255);")

        self.horizontalLayout_3.addWidget(self.widgetToolSpace_3)


        self.gridLayout_17.addWidget(self.widgetUp, 0, 0, 1, 1)

        icon7 = QIcon()
        icon7.addFile(u":/icon/icon/caret-right.svg", QSize(), QIcon.Normal, QIcon.On)
        self.tabWidgetModels.addTab(self.pageModel_1, icon7, "")
        self.pageModel_2 = QWidget()
        self.pageModel_2.setObjectName(u"pageModel_2")
        self.tabWidgetModels.addTab(self.pageModel_2, icon7, "")
        self.pageModel_3 = QWidget()
        self.pageModel_3.setObjectName(u"pageModel_3")
        self.tabWidgetModels.addTab(self.pageModel_3, icon7, "")

        self.verticalLayout.addWidget(self.tabWidgetModels)

        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)

        self.tabWidgetModels.setCurrentIndex(2)
        self.tabWidgetSide_4.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
#if QT_CONFIG(whatsthis)
        self.tabWidgetModels.setWhatsThis(QCoreApplication.translate("MainWindow", u"<html><head/><body><p>\u8fd9\u91cc\u662f\u5207\u6362\u6a21\u5757\u7684\u5bb9\u5668</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.tabWidgetSide_4.setTabText(self.tabWidgetSide_4.indexOf(self.widgetSide_7), QCoreApplication.translate("MainWindow", u"\u53c2\u65701", None))
        self.tabWidgetSide_4.setTabText(self.tabWidgetSide_4.indexOf(self.widgetSide_8), QCoreApplication.translate("MainWindow", u"\u53c2\u65702", None))
        self.tabWidget_17.setTabText(self.tabWidget_17.indexOf(self.tab_33), QCoreApplication.translate("MainWindow", u"Tab 1", None))
        self.tabWidget_17.setTabText(self.tabWidget_17.indexOf(self.tab_34), QCoreApplication.translate("MainWindow", u"Tab 2", None))
        self.tabWidget_19.setTabText(self.tabWidget_19.indexOf(self.tab_37), QCoreApplication.translate("MainWindow", u"Tab 1", None))
        self.tabWidget_19.setTabText(self.tabWidget_19.indexOf(self.tab_38), QCoreApplication.translate("MainWindow", u"Tab 2", None))
        self.tabWidget_4.setTabText(self.tabWidget_4.indexOf(self.tab_7), QCoreApplication.translate("MainWindow", u"\u8f93\u51fa", None))
        self.tabWidget_4.setTabText(self.tabWidget_4.indexOf(self.tab_8), QCoreApplication.translate("MainWindow", u"Tab 2", None))
#if QT_CONFIG(whatsthis)
        self.widgetGlobal_3.setWhatsThis(QCoreApplication.translate("MainWindow", u"<html><head/><body><p>\u8fd9\u91cc\u662f\u5168\u5c40\u4fe1\u606f</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.label_17.setText(QCoreApplication.translate("MainWindow", u"\u5168\u5c40\u4fe1\u606f\uff1a", None))
        self.label_16.setText(QCoreApplication.translate("MainWindow", u"\u5168\u5c40\u8bbe\u7f6e2\uff1a", None))
        self.label_15.setText(QCoreApplication.translate("MainWindow", u"\u5168\u5c40\u8bbe\u7f6e1\uff1a", None))
        self.label_18.setText(QCoreApplication.translate("MainWindow", u"\u4fe1\u606f\uff0c\u4fe1\u606f\uff0c\u4fe1\u606f", None))
        self.toolButton_15.setText(QCoreApplication.translate("MainWindow", u"\u6253\u5f00\u8ba1\u5212", None))
        self.toolButton_16.setText(QCoreApplication.translate("MainWindow", u"\u4fdd\u5b58", None))
        self.label_19.setText(QCoreApplication.translate("MainWindow", u"\u6587\u4ef6", None))
        self.toolButton_17.setText(QCoreApplication.translate("MainWindow", u"\u7f16\u8f91", None))
        self.toolButton_18.setText(QCoreApplication.translate("MainWindow", u"\u6267\u884c\u8ba1\u5212", None))
        self.label_20.setText(QCoreApplication.translate("MainWindow", u"\u8ba1\u5212", None))
        self.label_21.setText(QCoreApplication.translate("MainWindow", u"\u5176\u4ed6", None))
        self.toolButton_19.setText(QCoreApplication.translate("MainWindow", u"\u8bbe\u7f6e", None))
        self.toolButton_20.setText(QCoreApplication.translate("MainWindow", u"\u5e2e\u52a9", None))
        self.toolButton_21.setText(QCoreApplication.translate("MainWindow", u"\u6e05\u7a7a", None))
        self.tabWidgetModels.setTabText(self.tabWidgetModels.indexOf(self.pageModel_1), QCoreApplication.translate("MainWindow", u"\u6a21\u57571", None))
        self.tabWidgetModels.setTabText(self.tabWidgetModels.indexOf(self.pageModel_2), QCoreApplication.translate("MainWindow", u"\u6a21\u57572", None))
        self.tabWidgetModels.setTabText(self.tabWidgetModels.indexOf(self.pageModel_3), QCoreApplication.translate("MainWindow", u"\u6a21\u57573", None))
    # retranslateUi

