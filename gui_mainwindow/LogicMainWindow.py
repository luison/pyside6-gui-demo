from PySide6.QtWidgets import QMainWindow
from PySide6.QtGui import QIcon
from PySide6.QtCore import QSize
from gui_mainwindow.ui_MainWindow import Ui_MainWindow
from gui_plan.LogicPagePlan import LogicPagePlan

class LogicMainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self) -> None:
        super().__init__()
        self.setupUi(self)
        icon = QIcon()
        icon.addFile(u":/icon/icon/caret-right.svg", QSize(), QIcon.Normal, QIcon.On)
        self.pagePlan = LogicPagePlan()
        self.tabWidgetModels.addTab(self.pagePlan, icon, "Plan")
        self.tabWidgetModels.setCurrentIndex(3)