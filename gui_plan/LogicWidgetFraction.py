from PySide6.QtWidgets import QWidget, QLabel, QToolButton, QLineEdit, QCheckBox, QHBoxLayout
from gui_plan.ui_WidgetFraction import Ui_WidgetFraction

class GuiField():
    def __init__(self) -> None:
        self.label = None
        self.button = None
        self.file = None
        self.b10 = None
        self.exp = None
        self.gamma = None
        self.active = None
        self.widget = None
        self.layout = None
        return

class LogicWidgetFraction(QWidget, Ui_WidgetFraction):
    def __init__(self) -> None:
        super().__init__()
        self.setupUi(self)
        self.fields = []
        field1 = GuiField()
        field1.label = self.name_1
        field1.button = self.button_1
        field1.file = self.file_1
        field1.b10 = self.b10_1
        field1.exp = self.exp_1
        field1.gamma = self.gamma_1
        field1.active = self.active_1
        self.fields.append(field1)
        
    
    def addField(self):
        l = len(self.fields)
        field = GuiField()
        field.label = QLabel("Field "+str(l+1), self)
        field.widget = QWidget(self)
        field.layout = QHBoxLayout(field.widget)
        field.layout.setSpacing(0)
        field.layout.setContentsMargins(0, 0, 0, 0)
        field.layout.setSpacing(0)
        field.button = QToolButton(field.widget)
        field.file = QLineEdit(self.widget)
        field.layout.addWidget(field.button)
        field.layout.addWidget(field.file)
        field.b10 = QLineEdit(self)
        field.exp = QLineEdit(self)
        field.gamma = QLineEdit(self)
        field.active = QCheckBox("active", self)
        self.gridLayout.removeWidget(self.label_ab10)
        self.gridLayout.removeWidget(self.lineEdit_ae)
        self.gridLayout.removeWidget(self.lineEdit_agr)
        self.gridLayout.removeWidget(self.label_fe)
        self.gridLayout.removeWidget(self.label_eb10)
        self.gridLayout.removeWidget(self.lineEdit_ee)
        self.gridLayout.removeWidget(self.lineEdit_egr)
        self.gridLayout.removeWidget(self.label_fw)
        self.gridLayout.removeItem(self.verticalSpacer)
        self.gridLayout.addWidget(field.label, l+2, 0, 1, 1)
        self.gridLayout.addWidget(field.widget, l+2, 1, 1, 1)
        self.gridLayout.addWidget(field.b10, l+2, 2, 1, 1)
        self.gridLayout.addWidget(field.exp, l+2, 3, 1, 1)
        self.gridLayout.addWidget(field.gamma, l+2, 4, 1, 1)
        self.gridLayout.addWidget(field.active, l+2, 5, 1, 1)
        self.gridLayout.addWidget(self.label_ab10, l+3, 2, 1, 1)
        self.gridLayout.addWidget(self.lineEdit_ae, l+3, 3, 1, 1)
        self.gridLayout.addWidget(self.lineEdit_agr, l+3, 4, 1, 1)
        self.gridLayout.addWidget(self.label_fe, l+3, 5, 1, 1)
        self.gridLayout.addWidget(self.label_eb10, l+4, 2, 1, 1)
        self.gridLayout.addWidget(self.lineEdit_ee, l+4, 3, 1, 1)
        self.gridLayout.addWidget(self.lineEdit_egr, l+4, 4, 1, 1)
        self.gridLayout.addWidget(self.label_fw, l+4, 5, 1, 1)
        self.gridLayout.addItem(self.verticalSpacer, l+5, 2, 1, 1)
        self.fields.append(field) 
        return

    def deleteField(self):
        return 
