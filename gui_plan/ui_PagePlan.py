# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'PagePlan.ui'
##
## Created by: Qt User Interface Compiler version 6.4.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QFrame, QGridLayout, QGroupBox,
    QHBoxLayout, QLabel, QLineEdit, QPushButton,
    QScrollArea, QSizePolicy, QSpinBox, QSplitter,
    QTabWidget, QToolBox, QToolButton, QWidget)
import icon_rc

class Ui_PagePlan(object):
    def setupUi(self, PagePlan):
        if not PagePlan.objectName():
            PagePlan.setObjectName(u"PagePlan")
        PagePlan.resize(1273, 775)
        self.gridLayout_2 = QGridLayout(PagePlan)
        self.gridLayout_2.setSpacing(0)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.widgetUp = QWidget(PagePlan)
        self.widgetUp.setObjectName(u"widgetUp")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.widgetUp.sizePolicy().hasHeightForWidth())
        self.widgetUp.setSizePolicy(sizePolicy)
        self.widgetUp.setMinimumSize(QSize(0, 80))
        self.horizontalLayout_3 = QHBoxLayout(self.widgetUp)
        self.horizontalLayout_3.setSpacing(2)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalLayout_3.setContentsMargins(2, 2, 2, 2)
        self.widgetGlobal_3 = QWidget(self.widgetUp)
        self.widgetGlobal_3.setObjectName(u"widgetGlobal_3")
        sizePolicy1 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.widgetGlobal_3.sizePolicy().hasHeightForWidth())
        self.widgetGlobal_3.setSizePolicy(sizePolicy1)
        self.widgetGlobal_3.setMinimumSize(QSize(200, 0))
        self.widgetGlobal_3.setStyleSheet(u"background-color: rgb(255, 255, 255);")
        self.gridLayout_13 = QGridLayout(self.widgetGlobal_3)
        self.gridLayout_13.setObjectName(u"gridLayout_13")
        self.label_22 = QLabel(self.widgetGlobal_3)
        self.label_22.setObjectName(u"label_22")
        self.label_22.setEnabled(False)

        self.gridLayout_13.addWidget(self.label_22, 0, 1, 1, 1)

        self.label_18 = QLabel(self.widgetGlobal_3)
        self.label_18.setObjectName(u"label_18")

        self.gridLayout_13.addWidget(self.label_18, 2, 1, 1, 1)

        self.label_15 = QLabel(self.widgetGlobal_3)
        self.label_15.setObjectName(u"label_15")

        self.gridLayout_13.addWidget(self.label_15, 0, 0, 1, 1)

        self.label_17 = QLabel(self.widgetGlobal_3)
        self.label_17.setObjectName(u"label_17")

        self.gridLayout_13.addWidget(self.label_17, 2, 0, 1, 1)

        self.label_16 = QLabel(self.widgetGlobal_3)
        self.label_16.setObjectName(u"label_16")

        self.gridLayout_13.addWidget(self.label_16, 3, 0, 1, 1)

        self.lineEdit_3 = QLineEdit(self.widgetGlobal_3)
        self.lineEdit_3.setObjectName(u"lineEdit_3")

        self.gridLayout_13.addWidget(self.lineEdit_3, 3, 1, 1, 1)


        self.horizontalLayout_3.addWidget(self.widgetGlobal_3)

        self.widgetTool_7 = QWidget(self.widgetUp)
        self.widgetTool_7.setObjectName(u"widgetTool_7")
        sizePolicy1.setHeightForWidth(self.widgetTool_7.sizePolicy().hasHeightForWidth())
        self.widgetTool_7.setSizePolicy(sizePolicy1)
        self.widgetTool_7.setStyleSheet(u"background-color: rgb(255, 255, 255);")
        self.gridLayout_14 = QGridLayout(self.widgetTool_7)
        self.gridLayout_14.setSpacing(0)
        self.gridLayout_14.setObjectName(u"gridLayout_14")
        self.gridLayout_14.setContentsMargins(0, 0, 0, 0)
        self.toolButton_15 = QToolButton(self.widgetTool_7)
        self.toolButton_15.setObjectName(u"toolButton_15")
        self.toolButton_15.setMinimumSize(QSize(60, 60))
        self.toolButton_15.setCursor(QCursor(Qt.PointingHandCursor))
        icon = QIcon()
        icon.addFile(u":/icon/icon/folder.svg", QSize(), QIcon.Normal, QIcon.On)
        self.toolButton_15.setIcon(icon)
        self.toolButton_15.setIconSize(QSize(30, 30))
        self.toolButton_15.setPopupMode(QToolButton.MenuButtonPopup)
        self.toolButton_15.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolButton_15.setAutoRaise(True)

        self.gridLayout_14.addWidget(self.toolButton_15, 0, 0, 1, 1)

        self.toolButton_16 = QToolButton(self.widgetTool_7)
        self.toolButton_16.setObjectName(u"toolButton_16")
        self.toolButton_16.setEnabled(False)
        self.toolButton_16.setMinimumSize(QSize(60, 60))
        icon1 = QIcon()
        icon1.addFile(u":/icon/icon/save.svg", QSize(), QIcon.Normal, QIcon.Off)
        self.toolButton_16.setIcon(icon1)
        self.toolButton_16.setIconSize(QSize(30, 30))
        self.toolButton_16.setPopupMode(QToolButton.MenuButtonPopup)
        self.toolButton_16.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolButton_16.setAutoRaise(True)

        self.gridLayout_14.addWidget(self.toolButton_16, 0, 1, 1, 1)

        self.label_19 = QLabel(self.widgetTool_7)
        self.label_19.setObjectName(u"label_19")
        font = QFont()
        font.setFamilies([u"\u5fae\u8f6f\u96c5\u9ed1 Light"])
        font.setPointSize(8)
        self.label_19.setFont(font)
        self.label_19.setAlignment(Qt.AlignCenter)

        self.gridLayout_14.addWidget(self.label_19, 1, 0, 1, 2)


        self.horizontalLayout_3.addWidget(self.widgetTool_7)

        self.widgetTool_8 = QWidget(self.widgetUp)
        self.widgetTool_8.setObjectName(u"widgetTool_8")
        sizePolicy1.setHeightForWidth(self.widgetTool_8.sizePolicy().hasHeightForWidth())
        self.widgetTool_8.setSizePolicy(sizePolicy1)
        self.widgetTool_8.setStyleSheet(u"background-color: rgb(255, 255, 255);")
        self.gridLayout_15 = QGridLayout(self.widgetTool_8)
        self.gridLayout_15.setSpacing(0)
        self.gridLayout_15.setObjectName(u"gridLayout_15")
        self.gridLayout_15.setContentsMargins(0, 0, 0, 0)
        self.toolButton_17 = QToolButton(self.widgetTool_8)
        self.toolButton_17.setObjectName(u"toolButton_17")
        self.toolButton_17.setEnabled(False)
        self.toolButton_17.setMinimumSize(QSize(60, 60))
        icon2 = QIcon()
        icon2.addFile(u":/icon/icon/edit-1.svg", QSize(), QIcon.Normal, QIcon.On)
        self.toolButton_17.setIcon(icon2)
        self.toolButton_17.setIconSize(QSize(30, 30))
        self.toolButton_17.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolButton_17.setAutoRaise(True)

        self.gridLayout_15.addWidget(self.toolButton_17, 0, 1, 1, 1)

        self.toolButton_18 = QToolButton(self.widgetTool_8)
        self.toolButton_18.setObjectName(u"toolButton_18")
        self.toolButton_18.setEnabled(False)
        self.toolButton_18.setMinimumSize(QSize(60, 60))
        icon3 = QIcon()
        icon3.addFile(u":/icon/icon/play-circle-stroke.svg", QSize(), QIcon.Normal, QIcon.On)
        self.toolButton_18.setIcon(icon3)
        self.toolButton_18.setIconSize(QSize(30, 30))
        self.toolButton_18.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolButton_18.setAutoRaise(True)

        self.gridLayout_15.addWidget(self.toolButton_18, 0, 0, 1, 1)

        self.label_20 = QLabel(self.widgetTool_8)
        self.label_20.setObjectName(u"label_20")
        self.label_20.setFont(font)
        self.label_20.setAlignment(Qt.AlignCenter)

        self.gridLayout_15.addWidget(self.label_20, 1, 0, 1, 3)


        self.horizontalLayout_3.addWidget(self.widgetTool_8)

        self.widgetTool_9 = QWidget(self.widgetUp)
        self.widgetTool_9.setObjectName(u"widgetTool_9")
        sizePolicy1.setHeightForWidth(self.widgetTool_9.sizePolicy().hasHeightForWidth())
        self.widgetTool_9.setSizePolicy(sizePolicy1)
        self.widgetTool_9.setStyleSheet(u"background-color: rgb(255, 255, 255);")
        self.gridLayout_16 = QGridLayout(self.widgetTool_9)
        self.gridLayout_16.setSpacing(0)
        self.gridLayout_16.setObjectName(u"gridLayout_16")
        self.gridLayout_16.setContentsMargins(0, 0, 0, 0)
        self.toolButton_19 = QToolButton(self.widgetTool_9)
        self.toolButton_19.setObjectName(u"toolButton_19")
        self.toolButton_19.setMinimumSize(QSize(60, 60))
        icon4 = QIcon()
        icon4.addFile(u":/icon/icon/setting.svg", QSize(), QIcon.Normal, QIcon.On)
        self.toolButton_19.setIcon(icon4)
        self.toolButton_19.setIconSize(QSize(30, 30))
        self.toolButton_19.setPopupMode(QToolButton.DelayedPopup)
        self.toolButton_19.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolButton_19.setAutoRaise(True)

        self.gridLayout_16.addWidget(self.toolButton_19, 0, 1, 1, 1)

        self.toolButton_20 = QToolButton(self.widgetTool_9)
        self.toolButton_20.setObjectName(u"toolButton_20")
        self.toolButton_20.setMinimumSize(QSize(60, 60))
        icon5 = QIcon()
        icon5.addFile(u":/icon/icon/help-circle.svg", QSize(), QIcon.Normal, QIcon.On)
        self.toolButton_20.setIcon(icon5)
        self.toolButton_20.setIconSize(QSize(30, 30))
        self.toolButton_20.setPopupMode(QToolButton.DelayedPopup)
        self.toolButton_20.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolButton_20.setAutoRaise(True)

        self.gridLayout_16.addWidget(self.toolButton_20, 0, 4, 1, 1)

        self.label_21 = QLabel(self.widgetTool_9)
        self.label_21.setObjectName(u"label_21")
        self.label_21.setFont(font)
        self.label_21.setAlignment(Qt.AlignCenter)

        self.gridLayout_16.addWidget(self.label_21, 1, 1, 1, 4)

        self.toolButton_21 = QToolButton(self.widgetTool_9)
        self.toolButton_21.setObjectName(u"toolButton_21")
        self.toolButton_21.setEnabled(False)
        self.toolButton_21.setMinimumSize(QSize(60, 60))
        icon6 = QIcon()
        icon6.addFile(u":/icon/icon/clear.svg", QSize(), QIcon.Normal, QIcon.On)
        self.toolButton_21.setIcon(icon6)
        self.toolButton_21.setIconSize(QSize(30, 30))
        self.toolButton_21.setPopupMode(QToolButton.DelayedPopup)
        self.toolButton_21.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolButton_21.setAutoRaise(True)

        self.gridLayout_16.addWidget(self.toolButton_21, 0, 0, 1, 1)


        self.horizontalLayout_3.addWidget(self.widgetTool_9)

        self.widgetToolSpace_3 = QWidget(self.widgetUp)
        self.widgetToolSpace_3.setObjectName(u"widgetToolSpace_3")
        self.widgetToolSpace_3.setStyleSheet(u"background-color: rgb(255, 255, 255);")

        self.horizontalLayout_3.addWidget(self.widgetToolSpace_3)


        self.gridLayout_2.addWidget(self.widgetUp, 0, 0, 1, 1)

        self.splitter = QSplitter(PagePlan)
        self.splitter.setObjectName(u"splitter")
        self.splitter.setOrientation(Qt.Horizontal)
        self.splitter.setHandleWidth(1)
        self.tabWidgetSide = QTabWidget(self.splitter)
        self.tabWidgetSide.setObjectName(u"tabWidgetSide")
        self.tabWidgetSide.setEnabled(True)
        sizePolicy2 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy2.setHorizontalStretch(1)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.tabWidgetSide.sizePolicy().hasHeightForWidth())
        self.tabWidgetSide.setSizePolicy(sizePolicy2)
        self.tabWidgetSide.setMinimumSize(QSize(202, 0))
        self.tabWidgetSide.setTabPosition(QTabWidget.West)
        self.widgetSide_1 = QWidget()
        self.widgetSide_1.setObjectName(u"widgetSide_1")
        self.tabWidgetSide.addTab(self.widgetSide_1, "")
        self.widgetSide_2 = QWidget()
        self.widgetSide_2.setObjectName(u"widgetSide_2")
        self.tabWidgetSide.addTab(self.widgetSide_2, "")
        self.splitter.addWidget(self.tabWidgetSide)
        self.widget = QWidget(self.splitter)
        self.widget.setObjectName(u"widget")
        sizePolicy3 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy3.setHorizontalStretch(4)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.widget.sizePolicy().hasHeightForWidth())
        self.widget.setSizePolicy(sizePolicy3)
        self.gridLayout = QGridLayout(self.widget)
        self.gridLayout.setSpacing(0)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.splitter_5 = QSplitter(self.widget)
        self.splitter_5.setObjectName(u"splitter_5")
        self.splitter_5.setOrientation(Qt.Vertical)
        self.splitter_5.setHandleWidth(2)
        self.splitter_5.setChildrenCollapsible(False)
        self.splitter_4 = QSplitter(self.splitter_5)
        self.splitter_4.setObjectName(u"splitter_4")
        sizePolicy4 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy4.setHorizontalStretch(0)
        sizePolicy4.setVerticalStretch(3)
        sizePolicy4.setHeightForWidth(self.splitter_4.sizePolicy().hasHeightForWidth())
        self.splitter_4.setSizePolicy(sizePolicy4)
        self.splitter_4.setOrientation(Qt.Horizontal)
        self.splitter_4.setHandleWidth(2)
        self.splitter_4.setChildrenCollapsible(False)
        self.splitter_2 = QSplitter(self.splitter_4)
        self.splitter_2.setObjectName(u"splitter_2")
        sizePolicy5 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy5.setHorizontalStretch(3)
        sizePolicy5.setVerticalStretch(0)
        sizePolicy5.setHeightForWidth(self.splitter_2.sizePolicy().hasHeightForWidth())
        self.splitter_2.setSizePolicy(sizePolicy5)
        self.splitter_2.setOrientation(Qt.Vertical)
        self.splitter_2.setHandleWidth(2)
        self.splitter_2.setChildrenCollapsible(False)
        self.tabWidget_5 = QTabWidget(self.splitter_2)
        self.tabWidget_5.setObjectName(u"tabWidget_5")
        self.tabWidget_5.setStyleSheet(u"QTabWidget#tabWidget_5>QTabBar::tab{\n"
"height:18px\n"
"}")
        self.tabWidget_5.setMovable(False)
        self.tab_9 = QWidget()
        self.tab_9.setObjectName(u"tab_9")
        self.gridLayout_3 = QGridLayout(self.tab_9)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.spinBox_2 = QSpinBox(self.tab_9)
        self.spinBox_2.setObjectName(u"spinBox_2")

        self.gridLayout_3.addWidget(self.spinBox_2, 0, 3, 1, 1)

        self.spinBox = QSpinBox(self.tab_9)
        self.spinBox.setObjectName(u"spinBox")

        self.gridLayout_3.addWidget(self.spinBox, 0, 1, 1, 1)

        self.label_2 = QLabel(self.tab_9)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_3.addWidget(self.label_2, 0, 2, 1, 1)

        self.label = QLabel(self.tab_9)
        self.label.setObjectName(u"label")
        self.label.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_3.addWidget(self.label, 0, 0, 1, 1)

        self.pushButton = QPushButton(self.tab_9)
        self.pushButton.setObjectName(u"pushButton")

        self.gridLayout_3.addWidget(self.pushButton, 0, 4, 1, 1)

        self.groupBox = QGroupBox(self.tab_9)
        self.groupBox.setObjectName(u"groupBox")
        self.gridLayout_4 = QGridLayout(self.groupBox)
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.scrollArea = QScrollArea(self.groupBox)
        self.scrollArea.setObjectName(u"scrollArea")
        self.scrollArea.setFrameShape(QFrame.NoFrame)
        self.scrollArea.setFrameShadow(QFrame.Plain)
        self.scrollArea.setLineWidth(0)
        self.scrollArea.setWidgetResizable(True)
        self.scrollAreaWidgetContents = QWidget()
        self.scrollAreaWidgetContents.setObjectName(u"scrollAreaWidgetContents")
        self.scrollAreaWidgetContents.setGeometry(QRect(0, 0, 657, 416))
        self.scrollAreaWidgetContents.setStyleSheet(u"#scrollAreaWidgetContents {\n"
"}\n"
"")
        self.gridLayout_5 = QGridLayout(self.scrollAreaWidgetContents)
        self.gridLayout_5.setSpacing(2)
        self.gridLayout_5.setObjectName(u"gridLayout_5")
        self.gridLayout_5.setContentsMargins(2, 2, 2, 2)
        self.toolBox = QToolBox(self.scrollAreaWidgetContents)
        self.toolBox.setObjectName(u"toolBox")
        self.toolBox.setStyleSheet(u"QToolBox#toolBox::tab {\n"
"	background-color: rgb(255, 255, 255);\n"
"}")
        self.page = QWidget()
        self.page.setObjectName(u"page")
        self.page.setGeometry(QRect(0, 0, 653, 386))
        self.toolBox.addItem(self.page, u"Page 1")

        self.gridLayout_5.addWidget(self.toolBox, 0, 0, 1, 1)

        self.scrollArea.setWidget(self.scrollAreaWidgetContents)

        self.gridLayout_4.addWidget(self.scrollArea, 0, 0, 1, 1)


        self.gridLayout_3.addWidget(self.groupBox, 1, 0, 1, 5)

        self.tabWidget_5.addTab(self.tab_9, "")
        self.splitter_2.addWidget(self.tabWidget_5)
        self.splitter_4.addWidget(self.splitter_2)
        self.splitter_3 = QSplitter(self.splitter_4)
        self.splitter_3.setObjectName(u"splitter_3")
        sizePolicy6 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy6.setHorizontalStretch(10)
        sizePolicy6.setVerticalStretch(0)
        sizePolicy6.setHeightForWidth(self.splitter_3.sizePolicy().hasHeightForWidth())
        self.splitter_3.setSizePolicy(sizePolicy6)
        self.splitter_3.setOrientation(Qt.Vertical)
        self.splitter_3.setHandleWidth(2)
        self.splitter_3.setChildrenCollapsible(False)
        self.tabWidget_7 = QTabWidget(self.splitter_3)
        self.tabWidget_7.setObjectName(u"tabWidget_7")
        sizePolicy7 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy7.setHorizontalStretch(0)
        sizePolicy7.setVerticalStretch(0)
        sizePolicy7.setHeightForWidth(self.tabWidget_7.sizePolicy().hasHeightForWidth())
        self.tabWidget_7.setSizePolicy(sizePolicy7)
        self.tabWidget_7.setStyleSheet(u"QTabWidget#tabWidget_5>QTabBar::tab{\n"
"height:18px\n"
"}")
        self.tabWidget_7.setMovable(False)
        self.tab_13 = QWidget()
        self.tab_13.setObjectName(u"tab_13")
        self.label_3 = QLabel(self.tab_13)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setGeometry(QRect(30, 10, 53, 16))
        self.tabWidget_7.addTab(self.tab_13, "")
        self.splitter_3.addWidget(self.tabWidget_7)
        self.splitter_4.addWidget(self.splitter_3)
        self.splitter_5.addWidget(self.splitter_4)
        self.tabWidget = QTabWidget(self.splitter_5)
        self.tabWidget.setObjectName(u"tabWidget")
        sizePolicy8 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy8.setHorizontalStretch(0)
        sizePolicy8.setVerticalStretch(7)
        sizePolicy8.setHeightForWidth(self.tabWidget.sizePolicy().hasHeightForWidth())
        self.tabWidget.setSizePolicy(sizePolicy8)
        self.tab = QWidget()
        self.tab.setObjectName(u"tab")
        self.label_4 = QLabel(self.tab)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setGeometry(QRect(10, 20, 53, 16))
        self.tabWidget.addTab(self.tab, "")
        self.splitter_5.addWidget(self.tabWidget)

        self.gridLayout.addWidget(self.splitter_5, 0, 0, 1, 1)

        self.splitter.addWidget(self.widget)

        self.gridLayout_2.addWidget(self.splitter, 1, 0, 1, 1)


        self.retranslateUi(PagePlan)

        self.tabWidgetSide.setCurrentIndex(1)
        self.tabWidget_5.setCurrentIndex(0)
        self.toolBox.setCurrentIndex(0)
        self.toolBox.layout().setSpacing(2)
        self.tabWidget_7.setCurrentIndex(0)
        self.tabWidget.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(PagePlan)
    # setupUi

    def retranslateUi(self, PagePlan):
        PagePlan.setWindowTitle(QCoreApplication.translate("PagePlan", u"Plan", None))
#if QT_CONFIG(whatsthis)
        self.widgetGlobal_3.setWhatsThis(QCoreApplication.translate("PagePlan", u"<html><head/><body><p>\u8fd9\u91cc\u662f\u5168\u5c40\u4fe1\u606f</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.label_22.setText(QCoreApplication.translate("PagePlan", u" \u8bf7\u5bfc\u5165\u8ba1\u5212", None))
        self.label_18.setText("")
        self.label_15.setText(QCoreApplication.translate("PagePlan", u"\u75c5\u4ebaID\uff1a", None))
        self.label_17.setText(QCoreApplication.translate("PagePlan", u"\u6cbb\u7597\u65e5\u671f\uff1a", None))
        self.label_16.setText(QCoreApplication.translate("PagePlan", u"\u5168\u5c40\u8bbe\u7f6e2\uff1a", None))
#if QT_CONFIG(tooltip)
        self.toolButton_15.setToolTip(QCoreApplication.translate("PagePlan", u"\u6253\u5f00pan\u6587\u4ef6\u6216edit\u6587\u4ef6", None))
#endif // QT_CONFIG(tooltip)
        self.toolButton_15.setText(QCoreApplication.translate("PagePlan", u"\u6253\u5f00", None))
        self.toolButton_16.setText(QCoreApplication.translate("PagePlan", u"\u4fdd\u5b58", None))
        self.label_19.setText(QCoreApplication.translate("PagePlan", u"\u6587\u4ef6", None))
        self.toolButton_17.setText(QCoreApplication.translate("PagePlan", u"\u7f16\u8f91", None))
        self.toolButton_18.setText(QCoreApplication.translate("PagePlan", u"\u6267\u884c\u8ba1\u5212", None))
        self.label_20.setText(QCoreApplication.translate("PagePlan", u"\u8ba1\u5212", None))
        self.toolButton_19.setText(QCoreApplication.translate("PagePlan", u"\u8bbe\u7f6e", None))
        self.toolButton_20.setText(QCoreApplication.translate("PagePlan", u"\u5e2e\u52a9", None))
        self.label_21.setText(QCoreApplication.translate("PagePlan", u"\u5176\u4ed6", None))
        self.toolButton_21.setText(QCoreApplication.translate("PagePlan", u"\u6e05\u7a7a", None))
        self.tabWidgetSide.setTabText(self.tabWidgetSide.indexOf(self.widgetSide_1), QCoreApplication.translate("PagePlan", u"\u53c2\u65701", None))
        self.tabWidgetSide.setTabText(self.tabWidgetSide.indexOf(self.widgetSide_2), QCoreApplication.translate("PagePlan", u"\u53c2\u65702", None))
        self.label_2.setText(QCoreApplication.translate("PagePlan", u"\u7167\u5c04\u91ce\u6570\u91cf\uff1a", None))
        self.label.setText(QCoreApplication.translate("PagePlan", u"\u6cbb\u7597\u5468\u671f\uff1a", None))
        self.pushButton.setText(QCoreApplication.translate("PagePlan", u"\u786e\u8ba4", None))
        self.groupBox.setTitle(QCoreApplication.translate("PagePlan", u"\u8be6\u7ec6\u8bbe\u7f6e", None))
        self.toolBox.setItemText(self.toolBox.indexOf(self.page), QCoreApplication.translate("PagePlan", u"Page 1", None))
        self.tabWidget_5.setTabText(self.tabWidget_5.indexOf(self.tab_9), QCoreApplication.translate("PagePlan", u"Plan", None))
        self.label_3.setText(QCoreApplication.translate("PagePlan", u"TextLabel", None))
        self.tabWidget_7.setTabText(self.tabWidget_7.indexOf(self.tab_13), QCoreApplication.translate("PagePlan", u"Edit", None))
        self.label_4.setText(QCoreApplication.translate("PagePlan", u"TextLabel", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), QCoreApplication.translate("PagePlan", u"\u8f93\u51fa", None))
    # retranslateUi

