from PySide6.QtWidgets import QWidget, QHBoxLayout
from gui_plan.ui_PagePlan import Ui_PagePlan
from gui_plan.LogicWidgetFraction import LogicWidgetFraction

class LogicPagePlan(QWidget, Ui_PagePlan):
    def __init__(self) -> None:
        super().__init__()
        self.setupUi(self)
        self.fractions = []
        self.t = LogicWidgetFraction()
        layout = QHBoxLayout(self.page)
        layout.addWidget(self.t)
        self.page.setLayout(layout)
        self.toolBox.setItemText(0, "fraction 1")
        self.fractions.append(self.page)
        for i in range(10):
            self.addFraction()

        # self.toolBox.insertItem(1, self.t, "fraction 1")

    def addFraction(self):
        self.fractions.append(LogicWidgetFraction())
        self.toolBox.addItem(self.fractions[-1], "fraction "+ str(len(self.fractions)))