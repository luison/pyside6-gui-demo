# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'WidgetFraction.ui'
##
## Created by: Qt User Interface Compiler version 6.4.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QCheckBox, QGridLayout, QHBoxLayout,
    QLabel, QLineEdit, QSizePolicy, QSpacerItem,
    QToolButton, QWidget)

class Ui_WidgetFraction(object):
    def setupUi(self, WidgetFraction):
        if not WidgetFraction.objectName():
            WidgetFraction.setObjectName(u"WidgetFraction")
        WidgetFraction.resize(729, 192)
        self.gridLayout = QGridLayout(WidgetFraction)
        self.gridLayout.setObjectName(u"gridLayout")
        self.b10_1 = QLineEdit(WidgetFraction)
        self.b10_1.setObjectName(u"b10_1")

        self.gridLayout.addWidget(self.b10_1, 2, 2, 1, 1)

        self.label_ab10 = QLabel(WidgetFraction)
        self.label_ab10.setObjectName(u"label_ab10")

        self.gridLayout.addWidget(self.label_ab10, 3, 2, 1, 1)

        self.label_4 = QLabel(WidgetFraction)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setAlignment(Qt.AlignCenter)

        self.gridLayout.addWidget(self.label_4, 1, 1, 1, 1)

        self.label_5 = QLabel(WidgetFraction)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setAlignment(Qt.AlignCenter)

        self.gridLayout.addWidget(self.label_5, 1, 2, 1, 1)

        self.label_7 = QLabel(WidgetFraction)
        self.label_7.setObjectName(u"label_7")
        self.label_7.setAlignment(Qt.AlignCenter)

        self.gridLayout.addWidget(self.label_7, 1, 4, 1, 1)

        self.lineEdit_ae = QLineEdit(WidgetFraction)
        self.lineEdit_ae.setObjectName(u"lineEdit_ae")

        self.gridLayout.addWidget(self.lineEdit_ae, 3, 3, 1, 1)

        self.exp_1 = QLineEdit(WidgetFraction)
        self.exp_1.setObjectName(u"exp_1")

        self.gridLayout.addWidget(self.exp_1, 2, 3, 1, 1)

        self.label_fe = QLabel(WidgetFraction)
        self.label_fe.setObjectName(u"label_fe")

        self.gridLayout.addWidget(self.label_fe, 3, 5, 1, 1)

        self.label_6 = QLabel(WidgetFraction)
        self.label_6.setObjectName(u"label_6")
        self.label_6.setAlignment(Qt.AlignCenter)

        self.gridLayout.addWidget(self.label_6, 1, 3, 1, 1)

        self.name_1 = QLabel(WidgetFraction)
        self.name_1.setObjectName(u"name_1")

        self.gridLayout.addWidget(self.name_1, 2, 0, 1, 1)

        self.label_eb10 = QLabel(WidgetFraction)
        self.label_eb10.setObjectName(u"label_eb10")

        self.gridLayout.addWidget(self.label_eb10, 4, 2, 1, 1)

        self.widget = QWidget(WidgetFraction)
        self.widget.setObjectName(u"widget")
        self.widget.setMinimumSize(QSize(150, 0))
        self.horizontalLayout = QHBoxLayout(self.widget)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.button_1 = QToolButton(self.widget)
        self.button_1.setObjectName(u"button_1")

        self.horizontalLayout.addWidget(self.button_1)

        self.file_1 = QLineEdit(self.widget)
        self.file_1.setObjectName(u"file_1")

        self.horizontalLayout.addWidget(self.file_1)


        self.gridLayout.addWidget(self.widget, 2, 1, 1, 1)

        self.gamma_1 = QLineEdit(WidgetFraction)
        self.gamma_1.setObjectName(u"gamma_1")

        self.gridLayout.addWidget(self.gamma_1, 2, 4, 1, 1)

        self.lineEdit_ee = QLineEdit(WidgetFraction)
        self.lineEdit_ee.setObjectName(u"lineEdit_ee")

        self.gridLayout.addWidget(self.lineEdit_ee, 4, 3, 1, 1)

        self.label_fw = QLabel(WidgetFraction)
        self.label_fw.setObjectName(u"label_fw")

        self.gridLayout.addWidget(self.label_fw, 4, 5, 1, 1)

        self.lineEdit_agr = QLineEdit(WidgetFraction)
        self.lineEdit_agr.setObjectName(u"lineEdit_agr")

        self.gridLayout.addWidget(self.lineEdit_agr, 3, 4, 1, 1)

        self.active_1 = QCheckBox(WidgetFraction)
        self.active_1.setObjectName(u"active_1")

        self.gridLayout.addWidget(self.active_1, 2, 5, 1, 1)

        self.lineEdit_egr = QLineEdit(WidgetFraction)
        self.lineEdit_egr.setObjectName(u"lineEdit_egr")

        self.gridLayout.addWidget(self.lineEdit_egr, 4, 4, 1, 1)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout.addItem(self.verticalSpacer, 5, 2, 1, 1)

        self.checkBox = QCheckBox(WidgetFraction)
        self.checkBox.setObjectName(u"checkBox")
        self.checkBox.setTristate(False)

        self.gridLayout.addWidget(self.checkBox, 1, 0, 1, 1)


        self.retranslateUi(WidgetFraction)

        QMetaObject.connectSlotsByName(WidgetFraction)
    # setupUi

    def retranslateUi(self, WidgetFraction):
        WidgetFraction.setWindowTitle(QCoreApplication.translate("WidgetFraction", u"Form", None))
        self.label_ab10.setText(QCoreApplication.translate("WidgetFraction", u"Fraction Average B-10", None))
        self.label_4.setText(QCoreApplication.translate("WidgetFraction", u"dose file name(.rst)", None))
        self.label_5.setText(QCoreApplication.translate("WidgetFraction", u"B-10", None))
        self.label_7.setText(QCoreApplication.translate("WidgetFraction", u"Gamma Repair", None))
        self.label_fe.setText(QCoreApplication.translate("WidgetFraction", u"Fraction Exposure", None))
        self.label_6.setText(QCoreApplication.translate("WidgetFraction", u"Exposure", None))
        self.name_1.setText(QCoreApplication.translate("WidgetFraction", u"Field 1", None))
        self.label_eb10.setText(QCoreApplication.translate("WidgetFraction", u"Fraction Effective B-10", None))
        self.button_1.setText(QCoreApplication.translate("WidgetFraction", u"...", None))
        self.label_fw.setText(QCoreApplication.translate("WidgetFraction", u"Fraction Weight", None))
        self.active_1.setText(QCoreApplication.translate("WidgetFraction", u"Active", None))
        self.checkBox.setText(QCoreApplication.translate("WidgetFraction", u"same field", None))
    # retranslateUi

