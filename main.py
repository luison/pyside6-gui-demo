import sys
# add paths of modules to system's path
# import os
# file_path = os.path.abspath(__file__)
# root = "\\".join(file_path.split('\\')[:-1])
# sys.path.append(root+"\\resource")
from PySide6.QtWidgets import QApplication, QWidget
from gui_mainwindow.LogicMainWindow import LogicMainWindow
from gui_plan.LogicWidgetFraction import LogicWidgetFraction


if __name__ == "__main__":
    app = QApplication(sys.argv)
    win = LogicMainWindow()
    win.show()
    sys.exit(app.exec())
    