# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'MainWindow.ui'
##
## Created by: Qt User Interface Compiler version 6.3.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QComboBox, QGridLayout, QHBoxLayout,
    QLabel, QLineEdit, QMainWindow, QSizePolicy,
    QTabWidget, QToolButton, QWidget)
import icon_rc

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1025, 788)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.centralwidget.setStyleSheet(u"#centralwidget{\n"
"	background-color: rgb(0, 0, 0);\n"
"}")
        self.gridLayout = QGridLayout(self.centralwidget)
        self.gridLayout.setSpacing(0)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.tabWidgetModels = QTabWidget(self.centralwidget)
        self.tabWidgetModels.setObjectName(u"tabWidgetModels")
        self.tabWidgetModels.setContextMenuPolicy(Qt.NoContextMenu)
        self.tabWidgetModels.setStyleSheet(u"QTabWidget#tabWidgetModels>QTabBar::tab{\n"
"width: 150;\n"
"height:30;\n"
"}\n"
"QTabWidget>QTabBar::tab:!selected{\n"
"	background-color: rgb(243, 243, 243);\n"
"}")
        self.tabWidgetModels.setTabShape(QTabWidget.Rounded)
        self.tabWidgetModels.setElideMode(Qt.ElideLeft)
        self.tabWidgetModels.setUsesScrollButtons(True)
        self.tabWidgetModels.setDocumentMode(True)
        self.tabWidgetModels.setTabsClosable(False)
        self.tabWidgetModels.setMovable(False)
        self.tabWidgetModels.setTabBarAutoHide(False)
        self.pageModel_1 = QWidget()
        self.pageModel_1.setObjectName(u"pageModel_1")
        self.gridLayout_2 = QGridLayout(self.pageModel_1)
        self.gridLayout_2.setSpacing(0)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.widget_2 = QWidget(self.pageModel_1)
        self.widget_2.setObjectName(u"widget_2")
        self.gridLayout_5 = QGridLayout(self.widget_2)
        self.gridLayout_5.setSpacing(2)
        self.gridLayout_5.setObjectName(u"gridLayout_5")
        self.gridLayout_5.setContentsMargins(2, 0, 2, 0)
        self.tabWidget_3 = QTabWidget(self.widget_2)
        self.tabWidget_3.setObjectName(u"tabWidget_3")
        self.tab_5 = QWidget()
        self.tab_5.setObjectName(u"tab_5")
        self.tabWidget_3.addTab(self.tab_5, "")
        self.tab_6 = QWidget()
        self.tab_6.setObjectName(u"tab_6")
        self.tabWidget_3.addTab(self.tab_6, "")

        self.gridLayout_5.addWidget(self.tabWidget_3, 2, 1, 1, 2)

        self.tabWidget_2 = QTabWidget(self.widget_2)
        self.tabWidget_2.setObjectName(u"tabWidget_2")
        self.tabWidget_2.setStyleSheet(u"QTabWidget#tabWidget_2>QTabBar::tab{\n"
"height:18px\n"
"}\n"
"")
        self.tabWidget_2.setTabShape(QTabWidget.Rounded)
        self.tabWidget_2.setDocumentMode(False)
        self.tabWidget_2.setMovable(True)
        self.tab_3 = QWidget()
        self.tab_3.setObjectName(u"tab_3")
        self.tabWidget_2.addTab(self.tab_3, "")
        self.tab_4 = QWidget()
        self.tab_4.setObjectName(u"tab_4")
        self.tabWidget_2.addTab(self.tab_4, "")

        self.gridLayout_5.addWidget(self.tabWidget_2, 0, 1, 2, 1)

        self.tabWidget_6 = QTabWidget(self.widget_2)
        self.tabWidget_6.setObjectName(u"tabWidget_6")
        self.tabWidget_6.setStyleSheet(u"QTabWidget#tabWidget_6>QTabBar::tab{\n"
"height:18px\n"
"}")
        self.tab_11 = QWidget()
        self.tab_11.setObjectName(u"tab_11")
        self.tabWidget_6.addTab(self.tab_11, "")
        self.tab_12 = QWidget()
        self.tab_12.setObjectName(u"tab_12")
        self.tabWidget_6.addTab(self.tab_12, "")

        self.gridLayout_5.addWidget(self.tabWidget_6, 1, 2, 1, 1)

        self.tabWidgetSide = QTabWidget(self.widget_2)
        self.tabWidgetSide.setObjectName(u"tabWidgetSide")
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tabWidgetSide.sizePolicy().hasHeightForWidth())
        self.tabWidgetSide.setSizePolicy(sizePolicy)
        self.tabWidgetSide.setMinimumSize(QSize(202, 0))
        self.tabWidgetSide.setTabPosition(QTabWidget.West)
        self.widgetSide_1 = QWidget()
        self.widgetSide_1.setObjectName(u"widgetSide_1")
        self.tabWidgetSide.addTab(self.widgetSide_1, "")
        self.widgetSide_2 = QWidget()
        self.widgetSide_2.setObjectName(u"widgetSide_2")
        self.tabWidgetSide.addTab(self.widgetSide_2, "")

        self.gridLayout_5.addWidget(self.tabWidgetSide, 0, 0, 3, 1)

        self.tabWidget_5 = QTabWidget(self.widget_2)
        self.tabWidget_5.setObjectName(u"tabWidget_5")
        self.tabWidget_5.setStyleSheet(u"QTabWidget#tabWidget_5>QTabBar::tab{\n"
"height:18px\n"
"}")
        self.tabWidget_5.setMovable(False)
        self.tab_9 = QWidget()
        self.tab_9.setObjectName(u"tab_9")
        self.tabWidget_5.addTab(self.tab_9, "")
        self.tab_10 = QWidget()
        self.tab_10.setObjectName(u"tab_10")
        self.tabWidget_5.addTab(self.tab_10, "")

        self.gridLayout_5.addWidget(self.tabWidget_5, 0, 2, 1, 1)


        self.gridLayout_2.addWidget(self.widget_2, 2, 1, 1, 1)

        self.widgetUp = QWidget(self.pageModel_1)
        self.widgetUp.setObjectName(u"widgetUp")
        self.widgetUp.setMinimumSize(QSize(0, 80))
        self.horizontalLayout = QHBoxLayout(self.widgetUp)
        self.horizontalLayout.setSpacing(2)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(2, 2, 2, 2)
        self.widgetGlobal = QWidget(self.widgetUp)
        self.widgetGlobal.setObjectName(u"widgetGlobal")
        sizePolicy1 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.widgetGlobal.sizePolicy().hasHeightForWidth())
        self.widgetGlobal.setSizePolicy(sizePolicy1)
        self.widgetGlobal.setMinimumSize(QSize(200, 0))
        self.widgetGlobal.setStyleSheet(u"background-color: rgb(255, 255, 255);")
        self.gridLayout_6 = QGridLayout(self.widgetGlobal)
        self.gridLayout_6.setObjectName(u"gridLayout_6")
        self.label_3 = QLabel(self.widgetGlobal)
        self.label_3.setObjectName(u"label_3")

        self.gridLayout_6.addWidget(self.label_3, 0, 0, 1, 1)

        self.lineEdit = QLineEdit(self.widgetGlobal)
        self.lineEdit.setObjectName(u"lineEdit")

        self.gridLayout_6.addWidget(self.lineEdit, 1, 1, 1, 1)

        self.label_4 = QLabel(self.widgetGlobal)
        self.label_4.setObjectName(u"label_4")

        self.gridLayout_6.addWidget(self.label_4, 1, 0, 1, 1)

        self.comboBox = QComboBox(self.widgetGlobal)
        self.comboBox.setObjectName(u"comboBox")
        self.comboBox.setAutoFillBackground(False)

        self.gridLayout_6.addWidget(self.comboBox, 0, 1, 1, 1)

        self.label_5 = QLabel(self.widgetGlobal)
        self.label_5.setObjectName(u"label_5")

        self.gridLayout_6.addWidget(self.label_5, 2, 0, 1, 1)

        self.label_6 = QLabel(self.widgetGlobal)
        self.label_6.setObjectName(u"label_6")

        self.gridLayout_6.addWidget(self.label_6, 2, 1, 1, 1)


        self.horizontalLayout.addWidget(self.widgetGlobal)

        self.widgetTool_1 = QWidget(self.widgetUp)
        self.widgetTool_1.setObjectName(u"widgetTool_1")
        sizePolicy1.setHeightForWidth(self.widgetTool_1.sizePolicy().hasHeightForWidth())
        self.widgetTool_1.setSizePolicy(sizePolicy1)
        self.widgetTool_1.setStyleSheet(u"background-color: rgb(255, 255, 255);")
        self.gridLayout_3 = QGridLayout(self.widgetTool_1)
        self.gridLayout_3.setSpacing(0)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.gridLayout_3.setContentsMargins(0, 0, 0, 0)
        self.toolButton = QToolButton(self.widgetTool_1)
        self.toolButton.setObjectName(u"toolButton")
        self.toolButton.setMinimumSize(QSize(60, 60))
        icon = QIcon()
        icon.addFile(u":/icon/icon/folder.svg", QSize(), QIcon.Normal, QIcon.On)
        self.toolButton.setIcon(icon)
        self.toolButton.setIconSize(QSize(30, 30))
        self.toolButton.setPopupMode(QToolButton.MenuButtonPopup)
        self.toolButton.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolButton.setAutoRaise(True)

        self.gridLayout_3.addWidget(self.toolButton, 0, 0, 1, 1)

        self.toolButton_2 = QToolButton(self.widgetTool_1)
        self.toolButton_2.setObjectName(u"toolButton_2")
        self.toolButton_2.setMinimumSize(QSize(60, 60))
        icon1 = QIcon()
        icon1.addFile(u":/icon/icon/save.svg", QSize(), QIcon.Normal, QIcon.On)
        self.toolButton_2.setIcon(icon1)
        self.toolButton_2.setIconSize(QSize(30, 30))
        self.toolButton_2.setPopupMode(QToolButton.MenuButtonPopup)
        self.toolButton_2.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolButton_2.setAutoRaise(True)

        self.gridLayout_3.addWidget(self.toolButton_2, 0, 1, 1, 1)

        self.label = QLabel(self.widgetTool_1)
        self.label.setObjectName(u"label")
        font = QFont()
        font.setFamilies([u"\u5fae\u8f6f\u96c5\u9ed1 Light"])
        font.setPointSize(8)
        self.label.setFont(font)
        self.label.setAlignment(Qt.AlignCenter)

        self.gridLayout_3.addWidget(self.label, 1, 0, 1, 2)


        self.horizontalLayout.addWidget(self.widgetTool_1)

        self.widgetTool_2 = QWidget(self.widgetUp)
        self.widgetTool_2.setObjectName(u"widgetTool_2")
        sizePolicy1.setHeightForWidth(self.widgetTool_2.sizePolicy().hasHeightForWidth())
        self.widgetTool_2.setSizePolicy(sizePolicy1)
        self.widgetTool_2.setStyleSheet(u"background-color: rgb(255, 255, 255);")
        self.gridLayout_4 = QGridLayout(self.widgetTool_2)
        self.gridLayout_4.setSpacing(0)
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.gridLayout_4.setContentsMargins(0, 0, 0, 0)
        self.toolButton_4 = QToolButton(self.widgetTool_2)
        self.toolButton_4.setObjectName(u"toolButton_4")
        self.toolButton_4.setMinimumSize(QSize(60, 60))
        icon2 = QIcon()
        icon2.addFile(u":/icon/icon/edit-1.svg", QSize(), QIcon.Normal, QIcon.On)
        self.toolButton_4.setIcon(icon2)
        self.toolButton_4.setIconSize(QSize(30, 30))
        self.toolButton_4.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolButton_4.setAutoRaise(True)

        self.gridLayout_4.addWidget(self.toolButton_4, 0, 1, 1, 1)

        self.toolButton_3 = QToolButton(self.widgetTool_2)
        self.toolButton_3.setObjectName(u"toolButton_3")
        self.toolButton_3.setMinimumSize(QSize(60, 60))
        icon3 = QIcon()
        icon3.addFile(u":/icon/icon/play-circle-stroke.svg", QSize(), QIcon.Normal, QIcon.On)
        self.toolButton_3.setIcon(icon3)
        self.toolButton_3.setIconSize(QSize(30, 30))
        self.toolButton_3.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolButton_3.setAutoRaise(True)

        self.gridLayout_4.addWidget(self.toolButton_3, 0, 0, 1, 1)

        self.label_2 = QLabel(self.widgetTool_2)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setFont(font)
        self.label_2.setAlignment(Qt.AlignCenter)

        self.gridLayout_4.addWidget(self.label_2, 1, 0, 1, 3)


        self.horizontalLayout.addWidget(self.widgetTool_2)

        self.widgetTool_3 = QWidget(self.widgetUp)
        self.widgetTool_3.setObjectName(u"widgetTool_3")
        sizePolicy1.setHeightForWidth(self.widgetTool_3.sizePolicy().hasHeightForWidth())
        self.widgetTool_3.setSizePolicy(sizePolicy1)
        self.widgetTool_3.setStyleSheet(u"background-color: rgb(255, 255, 255);")
        self.gridLayout_7 = QGridLayout(self.widgetTool_3)
        self.gridLayout_7.setSpacing(0)
        self.gridLayout_7.setObjectName(u"gridLayout_7")
        self.gridLayout_7.setContentsMargins(0, 0, 0, 0)
        self.label_7 = QLabel(self.widgetTool_3)
        self.label_7.setObjectName(u"label_7")
        self.label_7.setFont(font)
        self.label_7.setAlignment(Qt.AlignCenter)

        self.gridLayout_7.addWidget(self.label_7, 1, 0, 1, 4)

        self.toolButton_6 = QToolButton(self.widgetTool_3)
        self.toolButton_6.setObjectName(u"toolButton_6")
        self.toolButton_6.setMinimumSize(QSize(60, 60))
        icon4 = QIcon()
        icon4.addFile(u":/icon/icon/setting.svg", QSize(), QIcon.Normal, QIcon.On)
        self.toolButton_6.setIcon(icon4)
        self.toolButton_6.setIconSize(QSize(30, 30))
        self.toolButton_6.setPopupMode(QToolButton.DelayedPopup)
        self.toolButton_6.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolButton_6.setAutoRaise(True)

        self.gridLayout_7.addWidget(self.toolButton_6, 0, 0, 1, 1)

        self.toolButton_7 = QToolButton(self.widgetTool_3)
        self.toolButton_7.setObjectName(u"toolButton_7")
        self.toolButton_7.setMinimumSize(QSize(60, 60))
        icon5 = QIcon()
        icon5.addFile(u":/icon/icon/help-circle.svg", QSize(), QIcon.Normal, QIcon.On)
        self.toolButton_7.setIcon(icon5)
        self.toolButton_7.setIconSize(QSize(30, 30))
        self.toolButton_7.setPopupMode(QToolButton.DelayedPopup)
        self.toolButton_7.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolButton_7.setAutoRaise(True)

        self.gridLayout_7.addWidget(self.toolButton_7, 0, 3, 1, 1)

        self.toolButton_8 = QToolButton(self.widgetTool_3)
        self.toolButton_8.setObjectName(u"toolButton_8")
        self.toolButton_8.setMinimumSize(QSize(60, 60))
        icon6 = QIcon()
        icon6.addFile(u":/icon/icon/clear.svg", QSize(), QIcon.Normal, QIcon.On)
        self.toolButton_8.setIcon(icon6)
        self.toolButton_8.setIconSize(QSize(30, 30))
        self.toolButton_8.setPopupMode(QToolButton.DelayedPopup)
        self.toolButton_8.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolButton_8.setAutoRaise(True)

        self.gridLayout_7.addWidget(self.toolButton_8, 0, 1, 1, 1)


        self.horizontalLayout.addWidget(self.widgetTool_3)

        self.widgetToolSpace = QWidget(self.widgetUp)
        self.widgetToolSpace.setObjectName(u"widgetToolSpace")
        self.widgetToolSpace.setStyleSheet(u"background-color: rgb(255, 255, 255);")

        self.horizontalLayout.addWidget(self.widgetToolSpace)


        self.gridLayout_2.addWidget(self.widgetUp, 0, 0, 1, 2)

        icon7 = QIcon()
        icon7.addFile(u"D:/Lenovo/Code/TPS/GUI/Resouce/file-add.svg", QSize(), QIcon.Normal, QIcon.On)
        self.tabWidgetModels.addTab(self.pageModel_1, icon7, "")
        self.pageModel_2 = QWidget()
        self.pageModel_2.setObjectName(u"pageModel_2")
        icon8 = QIcon()
        icon8.addFile(u"D:/Lenovo/Code/TPS/GUI/Resouce/save.svg", QSize(), QIcon.Normal, QIcon.On)
        self.tabWidgetModels.addTab(self.pageModel_2, icon8, "")
        self.pageModel_3 = QWidget()
        self.pageModel_3.setObjectName(u"pageModel_3")
        icon9 = QIcon()
        icon9.addFile(u"D:/Lenovo/Code/TPS/GUI/Resouce/back.svg", QSize(), QIcon.Normal, QIcon.On)
        self.tabWidgetModels.addTab(self.pageModel_3, icon9, "")
        self.pageModel_4 = QWidget()
        self.pageModel_4.setObjectName(u"pageModel_4")
        self.tabWidgetModels.addTab(self.pageModel_4, "")
        self.pageModel_5 = QWidget()
        self.pageModel_5.setObjectName(u"pageModel_5")
        self.tabWidgetModels.addTab(self.pageModel_5, "")

        self.gridLayout.addWidget(self.tabWidgetModels, 0, 0, 1, 1)

        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)

        self.tabWidgetModels.setCurrentIndex(0)
        self.tabWidget_3.setCurrentIndex(1)
        self.tabWidget_2.setCurrentIndex(0)
        self.tabWidgetSide.setCurrentIndex(1)


        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
#if QT_CONFIG(whatsthis)
        self.tabWidgetModels.setWhatsThis(QCoreApplication.translate("MainWindow", u"<html><head/><body><p>\u8fd9\u91cc\u662f\u5207\u6362\u6a21\u5757\u7684\u5bb9\u5668</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.tabWidget_3.setTabText(self.tabWidget_3.indexOf(self.tab_5), QCoreApplication.translate("MainWindow", u"\u9009\u62e9", None))
        self.tabWidget_3.setTabText(self.tabWidget_3.indexOf(self.tab_6), QCoreApplication.translate("MainWindow", u"\u8f93\u51fa", None))
        self.tabWidget_2.setTabText(self.tabWidget_2.indexOf(self.tab_3), QCoreApplication.translate("MainWindow", u"Plan Combination Utility", None))
        self.tabWidget_2.setTabText(self.tabWidget_2.indexOf(self.tab_4), QCoreApplication.translate("MainWindow", u"Plan Edit Tool", None))
        self.tabWidget_6.setTabText(self.tabWidget_6.indexOf(self.tab_11), QCoreApplication.translate("MainWindow", u"Tab 1", None))
        self.tabWidget_6.setTabText(self.tabWidget_6.indexOf(self.tab_12), QCoreApplication.translate("MainWindow", u"Tab 2", None))
        self.tabWidgetSide.setTabText(self.tabWidgetSide.indexOf(self.widgetSide_1), QCoreApplication.translate("MainWindow", u"\u53c2\u65701", None))
        self.tabWidgetSide.setTabText(self.tabWidgetSide.indexOf(self.widgetSide_2), QCoreApplication.translate("MainWindow", u"\u53c2\u65702", None))
        self.tabWidget_5.setTabText(self.tabWidget_5.indexOf(self.tab_9), QCoreApplication.translate("MainWindow", u"Tab 1", None))
        self.tabWidget_5.setTabText(self.tabWidget_5.indexOf(self.tab_10), QCoreApplication.translate("MainWindow", u"Tab 2", None))
#if QT_CONFIG(whatsthis)
        self.widgetGlobal.setWhatsThis(QCoreApplication.translate("MainWindow", u"<html><head/><body><p>\u8fd9\u91cc\u662f\u5168\u5c40\u4fe1\u606f</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.label_3.setText(QCoreApplication.translate("MainWindow", u"\u5168\u5c40\u8bbe\u7f6e1\uff1a", None))
        self.label_4.setText(QCoreApplication.translate("MainWindow", u"\u5168\u5c40\u8bbe\u7f6e2\uff1a", None))
        self.label_5.setText(QCoreApplication.translate("MainWindow", u"\u5168\u5c40\u4fe1\u606f\uff1a", None))
        self.label_6.setText(QCoreApplication.translate("MainWindow", u"\u4fe1\u606f\uff0c\u4fe1\u606f\uff0c\u4fe1\u606f", None))
        self.toolButton.setText(QCoreApplication.translate("MainWindow", u"\u6253\u5f00", None))
        self.toolButton_2.setText(QCoreApplication.translate("MainWindow", u"\u4fdd\u5b58", None))
        self.label.setText(QCoreApplication.translate("MainWindow", u"\u6587\u4ef6", None))
        self.toolButton_4.setText(QCoreApplication.translate("MainWindow", u"\u7f16\u8f91", None))
        self.toolButton_3.setText(QCoreApplication.translate("MainWindow", u"\u6267\u884c\u8ba1\u5212", None))
        self.label_2.setText(QCoreApplication.translate("MainWindow", u"\u8ba1\u5212", None))
        self.label_7.setText(QCoreApplication.translate("MainWindow", u"\u5176\u4ed6", None))
        self.toolButton_6.setText(QCoreApplication.translate("MainWindow", u"\u8bbe\u7f6e", None))
        self.toolButton_7.setText(QCoreApplication.translate("MainWindow", u"\u5e2e\u52a9", None))
        self.toolButton_8.setText(QCoreApplication.translate("MainWindow", u"\u6e05\u7a7a", None))
        self.tabWidgetModels.setTabText(self.tabWidgetModels.indexOf(self.pageModel_1), QCoreApplication.translate("MainWindow", u"\u6a21\u57571", None))
        self.tabWidgetModels.setTabText(self.tabWidgetModels.indexOf(self.pageModel_2), QCoreApplication.translate("MainWindow", u"\u6a21\u57572", None))
        self.tabWidgetModels.setTabText(self.tabWidgetModels.indexOf(self.pageModel_3), QCoreApplication.translate("MainWindow", u"\u6a21\u57573", None))
        self.tabWidgetModels.setTabText(self.tabWidgetModels.indexOf(self.pageModel_4), QCoreApplication.translate("MainWindow", u"\u6a21\u57574", None))
        self.tabWidgetModels.setTabText(self.tabWidgetModels.indexOf(self.pageModel_5), QCoreApplication.translate("MainWindow", u"\u6a21\u57575", None))
    # retranslateUi

